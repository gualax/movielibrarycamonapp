package certisur.com.movielibrarycamonapp.model.io;

import com.google.gson.Gson;

public class Movies {

    private int id;
    private String title;
    private String detail;
    private String img;


    public static Movies parse(String json) {
        return new Gson().fromJson(json, Movies.class);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }


    public String toJson(){
        Gson gson = new Gson();
        return gson.toJson(this);
    }

}
