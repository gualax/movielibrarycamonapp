package certisur.com.movielibrarycamonapp.model.io;

import java.util.ArrayList;

import certisur.com.movielibrarycamonapp.model.io.response.MovieResponse;
import retrofit2.Call;
import retrofit2.http.GET;

public interface MovieApiService {

    @GET("5c9e2a563000000e00ee970d")
    Call<ArrayList<Movies>>  getMovies();
}
