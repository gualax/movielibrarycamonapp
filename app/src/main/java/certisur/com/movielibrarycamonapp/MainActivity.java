package certisur.com.movielibrarycamonapp;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Window;

import java.util.ArrayList;

import certisur.com.movielibrarycamonapp.model.io.MovieApiAdapter;
import certisur.com.movielibrarycamonapp.model.io.Movies;
import certisur.com.movielibrarycamonapp.model.io.response.MovieResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements Callback<ArrayList<Movies>> {

    private RecyclerView mRecyclerView;
    private MoviesAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_movies);
        mRecyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        mAdapter = new MoviesAdapter(this);
        mRecyclerView.setAdapter(mAdapter);

        Call<ArrayList<Movies>>  call = MovieApiAdapter.getApiService().getMovies();
        call.enqueue(this);

    }


    @Override
    public void onResponse(Call<ArrayList<Movies>> call , Response<ArrayList<Movies>> response){
        Log.d("MovieLibrary", response.message());
        if(response.isSuccessful()){
            Log.d("MovieLibrary", "response.isSuccessful ");
            ArrayList<Movies> movies = response.body();
            Log.d("MovieLibrary", "Size of movies: " + movies.size());
            Log.d("MovieLibrary", "First title " + movies.get(0).getTitle());

            mAdapter.setData(movies);
        }
    }

    @Override
    public void onFailure(Call<ArrayList<Movies>> call, Throwable t) {
        Log.d("MovieLibrary", "onFailure" );
        Log.d("MovieLibrary", t.getMessage());

    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
        Log.d("MovieLibrary", "onPointerCaptureChanged" );

    }


}
