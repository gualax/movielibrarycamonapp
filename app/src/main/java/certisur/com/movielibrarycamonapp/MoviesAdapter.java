package certisur.com.movielibrarycamonapp;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import certisur.com.movielibrarycamonapp.model.io.Movies;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.ViewHolder> {

    private Context mainContext;
    private ArrayList<Movies> data;


    public static class ViewHolder extends RecyclerView.ViewHolder {

        // en este ejemplo cada elemento consta solo de un título
        public TextView textView;
        public ViewHolder(TextView tv) {
            super(tv);
            textView = tv;
        }
    }

    // Este es nuestro constructor (puede variar según lo que queremos mostrar)
    public MoviesAdapter(Context context) {
        Log.d("MovieLibrary","MoviesAdapter");
        data = new ArrayList<>();
        mainContext = context;
    }


    public  void setData(ArrayList<Movies> movieData) {
        Log.d("MovieLibrary","setData");

        data = movieData;
        notifyDataSetChanged();
    }

    @Override
    public MoviesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d("MovieLibrary","onCreateViewHolder");

        // Creamos una nueva vista
        TextView tv = (TextView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movies_view, parent, false);

        // Aquí podemos definir tamaños, márgenes, paddings
        // ...

        ViewHolder vh = new ViewHolder(tv);
        return vh;
    }


    // Este método reemplaza el contenido de cada view,
    // para cada elemento de la lista (nótese el argumento position)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Log.d("MovieLibrary","onBindViewHolder");
        final Movies selectedMovie = data.get(position);
        // - obtenemos un elemento del dataset según su posición
        // - reemplazamos el contenido de los views según tales datos
        Log.d("MovieLibrary",selectedMovie.getTitle());

        holder.textView.setText(selectedMovie.getTitle());


        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent intent = new Intent(mainContext,MoviesDetailView.class);
                    intent.putExtra("selectedMovie", selectedMovie.toJson());
                    mainContext.startActivity(intent);
            }
        });

    }

    // Método que define la cantidad de elementos del RecyclerView
    // Puede ser más complejo (por ejemplo si implementamos filtros o búsquedas)
    @Override
    public int getItemCount() {
        return data.size();
    }
}
