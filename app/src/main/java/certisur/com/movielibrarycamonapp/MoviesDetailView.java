package certisur.com.movielibrarycamonapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import certisur.com.movielibrarycamonapp.model.io.Movies;

public class MoviesDetailView extends AppCompatActivity {


    private Movies movie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies_detail_view);

        final TextView movieTitle = (TextView) this.findViewById(R.id.movieTitle);
        final ImageView movieImage = (ImageView) this.findViewById(R.id.movieImg);
        final TextView movieDetail = (TextView) this.findViewById(R.id.movieDetail);
        final Button playButton = (Button) this.findViewById(R.id.playButton);

        Bundle bundle = getIntent().getExtras();
        String jsonParams = bundle.getString("selectedMovie");
        Log.d("MovieLibrary", jsonParams);
        movie = Movies.parse(jsonParams);
        movieTitle.setText(movie.getTitle());
        movieDetail.setText(movie.getDetail());

        byte[] decodedString = Base64.decode(movie.getImg(), Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        movieImage.setImageBitmap(decodedByte);

    }
}
